package fr.imtaltantique.fila1.imtdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.imtaltantique.fila1.imtdb.entities.Actor;
import fr.imtaltantique.fila1.imtdb.entities.Movie;
import fr.imtaltantique.fila1.imtdb.repositories.ActorRepository;
import fr.imtaltantique.fila1.imtdb.repositories.MovieRepository;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.hypermedia.LinksSnippet;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ApiDocumentation {

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @SuppressWarnings("unused")
    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation)).build();
    }

    @Test
    public void movieListExample() throws Exception {
        this.movieRepository.deleteAll();
        List<Actor> starWars1Actors = createActors("Geraud Simon", "Romain Gervais");
        List<Actor> starWars8Actors = createActors("Florian Dussable");
        createMovie("Star Wars 1", "Not the best Star Wars", 4.0, starWars1Actors);
        createMovie("Star Wars 8", "Also not the best", 3.9, starWars8Actors);

        this.mockMvc.perform(get("/api/movies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].movieId", Matchers.iterableWithSize(2)))
                .andDo(document("movies-list-example",
                        responseFields(
                                subsectionWithPath("[]").description("An array of <<resources_movie, Movie resources>>")
                                        .optional().type(Movie.class)
                        )));
    }

    @Test
    public void movieByRateGreaterThanEqualExample() throws Exception {
        this.movieRepository.deleteAll();
        List<Actor> starWars1Actors = createActors("Geraud Simon", "Romain Gervais");
        List<Actor> starWars8Actors = createActors("Florian Dussable");
        createMovie("Star Wars 1", "Not the best Star Wars", 4.0, starWars1Actors);
        createMovie("Star Wars 8", "Also not the best", 3.9, starWars8Actors);
        createMovie("Star Wars 8", "Also not the best", 10.0, createActors("Simon SASSI",
                "Jean Pierre"));

        this.mockMvc.perform(get("/api/movies/search?rate=4.0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].rate", Matchers.everyItem(Matchers.greaterThanOrEqualTo(4.0))))
                .andDo(document("movies-search-by-rate-example",
                        requestParameters(
                                parameterWithName("rate").description("The minimum rate, inclusive")
                        ),
                        responseFields(
                                subsectionWithPath("[]").description("An array of <<resources_movie, Movie resources>>")
                                        .optional().type(Movie.class)
                        )));
    }

    @Test
    public void findByFirstNameAndLastNameAllIgnoreCaseExample() throws Exception {
        this.movieRepository.deleteAll();
        this.actorRepository.deleteAll();
        createActorsAndPersist("Geraud Simon", "Romain Gervais", "Raphael Wolfe");

        this.mockMvc.perform(get("/api/actors/search?firstName=Jean&lastName=Jack"))
                .andExpect(status().isNotFound());
        this.mockMvc.perform(get("/api/actors/search?firstName=Raphael&lastName=Wolfe"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", Matchers.hasToString("Raphael")))
                .andExpect(jsonPath("$.lastName", Matchers.hasToString("Wolfe")))
                .andDo(document("actors-search-by-name-example",
                        requestParameters(
                                parameterWithName("firstName").description("The first name of the actor"),
                                parameterWithName("lastName").description("The last name of the actor")
                        )
                ));
    }

    @Test
    public void addActorExample() throws Exception {
        this.movieRepository.deleteAll();
        this.movieRepository.flush();
        this.actorRepository.flush();

        this.mockMvc.perform(post("/api/movies/add"))
                .andExpect(status().isBadRequest());
        Movie starWars10 = new Movie("Star Wars 10", "Le pire", 0.1);
        starWars10.setActorsPlay(Arrays.asList(new Actor("Jean", "Jack"), new Actor("Hello", "You")));
        this.mockMvc.perform(post("/api/movies/add").contentType(MediaType.APPLICATION_JSON).content(asJsonString(starWars10)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", Matchers.hasToString("Star Wars 10")))
                .andExpect(jsonPath("$.actorsPlay", Matchers.iterableWithSize(2)))
                .andDo(document("actors-create-example",
                        requestFields(
                                getFieldDescriptors()
                        ),
                        responseFields(
                                getFieldDescriptors()
                        )
                ));
    }

    private FieldDescriptor[] getFieldDescriptors() {
        return new FieldDescriptor[]{fieldWithPath("movieId").description("Movie id"),
                fieldWithPath("title").description("Movie title"),
                fieldWithPath("synopsis").description("Movie synopsis"),
                fieldWithPath("rate").description("Movie rate"),
                fieldWithPath("actorsPlay[]").description("Movie actors").type(Actor.class),
                fieldWithPath("actorsPlay[].actorId").description("Actor id"),
                fieldWithPath("actorsPlay[].firstName").description("Actor first name"),
                fieldWithPath("actorsPlay[].lastName").description("Actor last name")};
    }

    private void createMovie(String title, String synopsis, Double rate, List<Actor> actors) {
        Movie movie = new Movie(title, synopsis, rate);
        movie.setTitle(title);
        movie.setActorsPlay(actors);

        this.movieRepository.saveAndFlush(movie);
    }

    private List<Actor> createActors(String... actorNames) {
        return Arrays.stream(actorNames).map(name -> {
            String[] splitted = name.split(" ");
            assert splitted.length == 2;
            return new Actor(splitted[0], splitted[1]);
        }).collect(Collectors.toList());
    }

    private void createActorsAndPersist(String... actorNames) {
       this.actorRepository.saveAll(this.createActors(actorNames));
       this.actorRepository.flush();
    }



    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
