package fr.imtaltantique.fila1.imtdb.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import java.util.List;

@Entity
@Table(name = "movies")
public class Movie {
    public Movie(String title, String synopsis, @DecimalMin("0.0") @DecimalMax("10.0") Double rate) {
        this.title = title;
        this.synopsis = synopsis;
        this.rate = rate;
    }

    public Movie() {}

    /**
     * Id du film dans la table de BDD Movies
     * Généré par auto-incrémentation par le SQL
     */
    @Id
    @GeneratedValue()
    @Column(name="movie_id")

    private Long movieId;

    /**
     * Titre du film dans la table de BDD Movies
     */
    @NotNull
    private String title;

    /**
     * Synopsis du film dans la table de BDD Actors
     */
    private String synopsis;

    /**
     * Rating du film dans la table de BDD Actors
     */
    @Column(name="rate")
    @DecimalMin("0.0")
    @DecimalMax("10.0")
    private Double rate;

    /**
     * Liaison avec la table Actors par "Un film est joué par un ou plusieurs acteurs"
     */

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name="plays",
            joinColumns = @JoinColumn(name="movie_id"),
            inverseJoinColumns = @JoinColumn(name="actor_id")
    )
    @JsonIgnoreProperties("moviesPlayed")
    private List<Actor> actorsPlay;



    /**
     * @return the movie_id
     */
    public Long getMovieId() {
        return movieId;
    }

    /**
     * @param movieId the movie_id to set
     */
    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the synopsis
     */
    public String getSynopsis() {
        return synopsis;
    }

    /**
     * @param synopsis the synopsis to set
     */
    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    /**
     * @return the rate
     */
    public Double getRate() {
        return rate;
    }

    /**
     * @param rate the synopsis to set
     */
    public void setRate(Double rate) {
        this.rate = rate;
    }

    /**
     * @return the actors_plays
     */
    public List<Actor> getActorsPlay() {
        return actorsPlay;
    }

    /**
     * @param actorsPlay the actors_play to set
     */
    public void setActorsPlay(List<Actor> actorsPlay) {
        actorsPlay.forEach((actor -> actor.addMovie(this)));
        this.actorsPlay = actorsPlay;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieId=" + movieId +
                ", title='" + title + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", rate=" + rate +
                '}';
    }
}
