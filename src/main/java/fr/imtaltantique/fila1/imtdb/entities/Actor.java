package fr.imtaltantique.fila1.imtdb.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "actors")
public class Actor {
    /**
     * Id de l'acteur dans la table de BDD Actors
     * Généré par auto-incrémentation par le SQL
     */
    @Id
    @GeneratedValue()
    @Column(name = "actor_id")
    private Long actorId;

    public Actor() {
    }

    /**
     * Prénom de l'acteur dans la table de BDD Actors
     */
    private String firstName;

    /**
     * Nom de l'acteur dans la table de BDD Actors
     */
    private String lastName;

    public Actor(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Liaison avec la table Movie par "Un acteur a joué dans un ou plusieurs films"
     */
    @ManyToMany(mappedBy = "actorsPlay")
    @JsonIgnoreProperties("actorsPlay")
    private List<Movie> moviesPlayed = new ArrayList<>();


    public void addMovie(Movie movie) {
        if (this.moviesPlayed.contains(movie)) {
            return;
        }
        this.moviesPlayed.add(movie);
    }

    /**
     * @return the actor_id
     */
    public Long getActorId() {
        return actorId;
    }

    /**
     * @param actorId the actor_id to set
     */
    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    /**
     * @return the actorId
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the first_name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the last_name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the first_name to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the movies_played
     */
    public List<Movie> getMoviesPlayed() {
        return moviesPlayed;
    }

    /**
     * @param moviesPlayed the first_name to set
     */
    public void setMoviesPlayed(List<Movie> moviesPlayed) {
        this.moviesPlayed = moviesPlayed;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "actorId=" + actorId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
