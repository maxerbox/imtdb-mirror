package fr.imtaltantique.fila1.imtdb.services;

import fr.imtaltantique.fila1.imtdb.entities.Movie;
import fr.imtaltantique.fila1.imtdb.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

@org.springframework.stereotype.Service
public class MovieService {
    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    public List<Movie> findAllMovies() {
        return movieRepository.findAll();
    }

    @Transactional(rollbackOn = Exception.class)
    public List<Movie> findByRateGreaterThanEqual(Double rate){
        return movieRepository.findByRateGreaterThanEqual(rate);
    }

    @Transactional(rollbackOn = Exception.class)
    public Movie addMovie(Movie movie){
        movieRepository.save(movie);
        return movie;
    }
}
