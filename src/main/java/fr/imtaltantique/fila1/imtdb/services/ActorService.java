package fr.imtaltantique.fila1.imtdb.services;

import fr.imtaltantique.fila1.imtdb.entities.Actor;
import fr.imtaltantique.fila1.imtdb.repositories.ActorRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ActorService {
    private final ActorRepository actorRepository;
    public ActorService(ActorRepository actorRepository){
        this.actorRepository = actorRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    public Optional<Actor> findTopByFirstNameAndLastNameAllIgnoreCase(String firstName, String lastName) {
        return actorRepository.findTopByFirstNameAndLastNameAllIgnoreCase(firstName, lastName);
    }
}
