package fr.imtaltantique.fila1.imtdb.repositories;

import fr.imtaltantique.fila1.imtdb.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Liste des requêtes SQL liées à la gestion de la classe Movie
 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.named-queries
 */

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    /*List<Movie> findAll();*/

    //Se base sur le saveAndFlush

    //idem

    /**
     * Récupérer la liste des films avec une note >= à une valeur
     */
    List<Movie> findByRateGreaterThanEqual(Double rate);



}
