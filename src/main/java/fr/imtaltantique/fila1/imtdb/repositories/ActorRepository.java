package fr.imtaltantique.fila1.imtdb.repositories;

import fr.imtaltantique.fila1.imtdb.entities.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Long> {

    /**
     * Récupérer un acteur et la liste de ses films
     *
     * Ajouter un .getMoviesPlayed pour récupérer la liste
     */
    Optional<Actor> findTopByFirstNameAndLastNameAllIgnoreCase(String firstName, String lastName);
}

