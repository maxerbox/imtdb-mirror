package fr.imtaltantique.fila1.imtdb.controllers;

import fr.imtaltantique.fila1.imtdb.entities.Movie;
import fr.imtaltantique.fila1.imtdb.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/movies")
public class MovieController {
    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * Le responseEntity va gérer la réponse HTTP
     * https://www.baeldung.com/spring-response-entity
     */
    @GetMapping
    public ResponseEntity<List<Movie>> findAllMovies() {
        return ResponseEntity.ok(movieService.findAllMovies());
    }

    @GetMapping("search")
    public ResponseEntity<List<Movie>> findByRateGreaterThanEqual(@RequestParam(name = "rate") Double rate){
        return ResponseEntity.ok(movieService.findByRateGreaterThanEqual(rate));
    }

    @PostMapping("add")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        return ResponseEntity.ok(movieService.addMovie(movie));
    }
}