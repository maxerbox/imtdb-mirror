package fr.imtaltantique.fila1.imtdb.controllers;

import fr.imtaltantique.fila1.imtdb.entities.Actor;
import fr.imtaltantique.fila1.imtdb.services.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("CommentedOutCode")
@RestController
@RequestMapping("/api/actors")
public class ActorController {
    private final ActorService actorService;

    @Autowired
    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    /*@GetMapping("findByName")
    public ResponseEntity<List<String>> findByFirstNameAndLastNameAllIgnoreCase(@RequestParam(name="firstName") String firstName, @RequestParam(name="lastName") String lastName) {
        Optional<Actor> actorSearched = actorRepository.findByFirstNameAndLastNameAllIgnoreCase(firstName, lastName);

        Actor actor = actorSearched.get();
        List<Movie> listMovies = actor.getMoviesPlayed(); //Renvoie la liste des films avec leurs acteurs
        List<String> listTitlesMovies = new ArrayList<>(); //Renvoie la liste des films

        for(Movie movie : listMovies)
            listTitlesMovies.add(movie.getTitle());

        return ResponseEntity.ok(listTitlesMovies);
    }*/

    /**
     * Récupère un acteur par nom+prénom
     *
     *
     * Test par : http://localhost:8080/api/actors/search?firstName=Raphael&lastName=Wolfe
     * avec en exemple "Raphael Wolfe"
     */
    @GetMapping("search")
    public ResponseEntity<Actor> findTopByFirstNameAndLastNameAllIgnoreCase(@RequestParam(name="firstName") String firstName, @RequestParam(name="lastName") String lastName) {
        return actorService.findTopByFirstNameAndLastNameAllIgnoreCase(firstName, lastName)
        .map(actor -> ResponseEntity.ok().body(actor))
        .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
